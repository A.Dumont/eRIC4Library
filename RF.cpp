/*
 * Library for controlling the eRIC4 module from LPRS
 * If your module version is 1.5 or above, you can define V_1_5 and have access to more functions
 * These functions are only supported by v 1.5
 * All the functions have not been implemented, see the eRIC4 datasheet for more information
 * 
 * By : Antoine Dumont
 * Date : June 4th, 2018
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <antoine.dumont@etu.uca.fr> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Antoine Dumont
 * ----------------------------------------------------------------------------
 *
 * Available functions :
 *
 *	For the setting
 *
 *	init
 *	setDefaultSettings
 *	enableCheckSettings
 *	enableRetry
 *	beginDebug
 *	setUARTBaudRate
 *	getUARTBaudRate
 *	setDataBaudRate
 *	getDataBaudRate
 *	setTransmitPower
 *	getTransmitPower
 *	setChannel
 *	getChannel
 *	setReceivePowerSaving
 *	getReceivePowerSaving
 *	setTransmitPowerSaving
 *	getTransmitPowerSaving
 *
 *	For v.1.5 only
 *
 *	setFrequency
 *	getFrequency
 *	setAESEncryptionMode
 *	getAESEncryptionMode
 *	setLowPowerMode
 *	getPowerMode
 *
 *	Functions used to send the command
 *
 *	changeSetting
 *	sendCommand
 *
 *
 * See RF.h for the defined parameters these functions can take
 * If you have any trouble changing the settings of the module, using the enableRetry is advised
 * If you want to be sure a setting really changed the way you wanted, using the enableCheckSettings is advised
 *
 */

#include "RF.h"

//Constructor
RF::RF(){
	init();
}

RF::RF(BufferedSerial * serial){

	_RFSerial = serial;
	init();
}

//Initialize all the attributes
void RF::init(){

	_checkSetting = false;
	_printDebug = false;
	_retry = 1;
	_UARTBaudRate = getUARTBaudRate();
	_dataRate = getDataBaudRate();
	_transmitPower = getTransmitPower();
	_channel = getChannel();
	_receivePowerSaving = getReceivePowerSaving();
	_transmitPowerSaving = getTransmitPowerSaving();
	#ifdef V_1_5
	_frequency = getFrequency();
	_lowPowerMode = getPowerMode();
	_AESEncryptionMode = getAESEncryptionMode();
	#endif
}

//Restore the default settings
bool RF::setDefaultSettings(){

	return 	setUARTBaudRate(UART_BAUD_19200)
			&& setDataBaudRate(DATA_BAUD_38400)
			&& setTransmitPower(TRANSMIT_POWER_9)
			&& setReceivePowerSaving(POWER_100)
			&& setTransmitPowerSaving(POWER_100)
			&& setChannel(CHANNEL_0);
}

//Enable checking if the parameters has been set as wanted
void RF::enableCheckSettings(){
	_checkSetting = true;
}

//If a command failed, enable to send the command again "retry" times
void RF::enableRetry(uint8_t retry){
	_retry = retry;
}

//Print debug messages on chosen serial port
void RF::beginDebug(Serial * s){
	_debugSerial=s;
	_printDebug = true;
}

//Set the UART baud rate
bool RF::setUARTBaudRate(uint8_t UARTBaudRate){

	if(changeSetting(UART_BAUD_ID, UARTBaudRate)){
		_UARTBaudRate = UARTBaudRate;
		return true;
	}
	else{
		return false;
	}
}

//Get the UART baud rate
uint8_t RF::getUARTBaudRate(){

	return _UARTBaudRate;
}

//Set the on-air data baud rate
bool RF::setDataBaudRate(uint8_t DataBaudRate){

	if(changeSetting(DATA_BAUD_ID, DataBaudRate)){
		_dataRate = DataBaudRate;
		return true;
	}
	else{
		return false;
	}
}

//Get the on-air data baud rate
uint8_t RF::getDataBaudRate(){

	return _dataRate;
}

//Set the RF power output value
bool RF::setTransmitPower(uint8_t transmitPower){

	if(changeSetting(TRANSMIT_POWER_ID, transmitPower)){
		_transmitPower = transmitPower;
		return true;
	}
	else{
		return false;
	}
}

//Get the RF power output value
uint8_t RF::getTransmitPower(){

	return _transmitPower;
}

//Set the channel number
bool RF::setChannel(uint8_t channel){

	if(changeSetting(CHANNEL_ID, channel)){
		_channel = channel;
		return true;
	}
	else{
		return false;
	}
}

//Get the channel number
uint8_t RF::getChannel(){

	return _channel;
}

//Set the average receiver current value
bool RF::setReceivePowerSaving(uint8_t receivePowerSaving){

	if(changeSetting(RECEIVER_POWER_SAVING_ID, receivePowerSaving)){
		_receivePowerSaving = receivePowerSaving;
		return true;
	}
	else{
		return false;
	}
}

//Get the average receiver current value
float RF::getReceivePowerSaving(){

	return _receivePowerSaving;
}

//Set the average transmitter current value
//The value set must be equal or greater than the one set for the receiver
bool RF::setTransmitPowerSaving(uint8_t transmitPowerSaving){

	if(changeSetting(TRANSMITTER_POWER_SAVING_ID, transmitPowerSaving)){
		_transmitPowerSaving = transmitPowerSaving;
		return true;
	}
	else{
		return false;
	}
}

//Get the average receiver current value
float RF::getTransmitPowerSaving(){

	return _transmitPowerSaving;
}


#ifdef V_1_5
//Set frequency in Hz
bool RF::setFrequency(unsigned int frequency){
	char tmp[30];

	sprintf(tmp, "%x", frequency);
	std::string hexFreq(tmp);

	if(changeSetting(FREQUENCY_ID, hexFreq)){
		_frequency = frequency;
		return true;
	}
	else{
		return false;
	}
}

//Get frequency
long RF::getFrequency(){

	return _frequency;
}

//Set low power mode
//See RF.h for more details about the modes available
bool RF::setLowPowerMode(uint8_t powerMode){

	if(changeSetting(LOW_POWER_MODE_ID, powerMode)){
		_lowPowerMode = powerMode;
		return true;
	}
	else{
		return false;
	}
}

//Get the power mode
uint8_t RF::getPowerMode(){

	return _lowPowerMode;
}

//Set AES encryption
bool RF::setAESEncryptionMode(uint8_t AESMode, std::string key){
	std::string param;
	param+=AESMode;
	param+=key;

	if(changeSetting(AES_ID, param)){
		_AESEncryptionMode = AESMode;
		return true;
	}
	else{
		return false;
	}
}

//Get AES encryption mode
uint8_t RF::getAESEncryptionMode(){

	return _AESEncryptionMode;
}
#endif

//Build the command to send and manage the retries
bool RF::changeSetting(uint8_t id, uint8_t param){
	//Build the command
	_cmd = CMD_PREFIX;
	_cmd += id;
	_cmd += param;

	int i(0);
	while(i<_retry && !sendCommand(std::string(1, id))) {i++;}
	return i<_retry;
}

bool RF::changeSetting(uint8_t id, std::string param){
	//Build the command
	_cmd = CMD_PREFIX;
	_cmd += id;
	_cmd += param;

	int i(0);
	while(i<_retry && !sendCommand(std::string(1, id))) {i++;}
	return i<_retry;
}

bool RF::changeSetting(std::string id, uint8_t param){
	//Build the command
	_cmd = CMD_PREFIX;
	_cmd += id;
	_cmd += param;

	int i(0);
	while(i<_retry && !sendCommand(id)) {i++;}
	return i<_retry;
}

bool RF::changeSetting(std::string id, std::string param){
	//Build the command
	_cmd = CMD_PREFIX;
	_cmd += id;
	_cmd += param;

	int i(0);
	while(i<_retry && !sendCommand(id)) {i++;}
	return i<_retry;
}

//Send the command to the module
//Manage the check of the settings
bool RF::sendCommand(std::string id){
	std::string response;

	while(_RFSerial->readable()) _RFSerial->getc(); //Flush the RX buffer

	if (_printDebug) _debugSerial->printf("Command sent : %s\r\n", _cmd.c_str());
	_RFSerial->puts(_cmd.c_str()); //Send the command to the module

	Timer t;
	t.start();
	uint32_t startTime = t.read_ms();
	while(!_RFSerial->readable()){ //Wait for the response
		if(t.read_ms() - startTime > TIMEOUT){
			if(_printDebug) _debugSerial->printf("Timeout : module failed to respond\r\n");
			return false;
		}
		wait_ms(1);
	}

	while(_RFSerial->readable() && response.length()<_cmd.length()){ //Get the response from the module
		response+=_RFSerial->getc();
		wait_ms(10);
	}
	if(_printDebug) _debugSerial->printf("Response : %s\r\n", response.c_str());
	
	if(response==_cmd){
		_RFSerial->puts(ACK); //Send the ACK string

		if(_checkSetting){
			//Build the command to get the current setting
			std::string cmdCheck = CMD_PREFIX;
			cmdCheck += id;
			cmdCheck += '?';
			response = "";
			
			while(_RFSerial->readable()) _RFSerial->getc(); //Flush the RX buffer
			
			if (_printDebug) _debugSerial->printf("Command sent : %s\r\n", cmdCheck.c_str());
			_RFSerial->puts(cmdCheck.c_str()); //Ask the module for the current setting value to check if the change occured

			startTime = t.read_ms();
			while(!_RFSerial->readable()){ //Wait for the response
				if(t.read_ms() - startTime > TIMEOUT){
					if(_printDebug) _debugSerial->printf("Timeout : module failed to respond\r\n");
					return false;
				}
				wait_ms(1);
			}
			
			while(_RFSerial->readable() && response.length()<_cmd.length()){ //Get the response from the module
				response+=_RFSerial->getc();
				wait_ms(10);
			}

			if(_printDebug) _debugSerial->printf("Response : %s\r\n", response.c_str());

			if(_printDebug) _debugSerial->printf((response==_cmd)?"Command sent and checked successfully\r\n":"Command check failed\r\n");
			return (response==_cmd)?true:false;
		}

		if(_printDebug) _debugSerial->printf("Command sent successfully\r\n");
		return true;
	}
	else{

		if(_printDebug) _debugSerial->printf("Command sending failed\r\n");
		return false;
	}
}

//Send a string via RF
void RF::sendFrame(std::string frame){
	_RFSerial->puts(frame.c_str());
}
