/*
 * Library for controlling the eRIC4 module from LPRS
 * If your module version is 1.5 or above, you can define V_1_5 and have access to more functions
 * These functions are only supported by v 1.5
 * All the functions have not been implemented, see the eRIC4 datasheet for more information
 * 
 * By : Antoine Dumont
 * Date : June 4th, 2018
 * 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <antoine.dumont@etu.uca.fr> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. Antoine Dumont
 * ----------------------------------------------------------------------------
 */

#include <mbed.h>
#include <stdlib.h>
#include <string>
#include "BufferedSerial.h"

#define TIMEOUT 500
//Some functions and parameters are available for the version 1.5 of the module
//#define V_1_5

#define MAX_CMD_SIZE 255

#define CMD_PREFIX "ER_CMD#"
#define ACK "ACK"

//UART data rate settings definition
#define UART_BAUD_ID 'U'
#define UART_BAUD_2400 '1'
#define UART_BAUD_4800 '2'
#define UART_BAUD_9600 '3'
#define UART_BAUD_19200 '4' //DEFAULT
#define UART_BAUD_38400 '5'
#define UART_BAUD_115200 '8'

//Over-air data rate settings definition
#define DATA_BAUD_ID 'B'
#define DATA_BAUD_1200 '0'
#define DATA_BAUD_2400 '1'
#define DATA_BAUD_4800 '2'
#define DATA_BAUD_9600 '3'
#define DATA_BAUD_19200 '4'
#define DATA_BAUD_38400 '5' //DEFAULT
#define DATA_BAUD_76800 '6'
#define DATA_BAUD_100000 '7'
#define DATA_BAUD_250000 '8'
#define DATA_BAUD_500000 '9'

//Transmit power settings definition
//The values are the powers of the output signal in dBm
#define TRANSMIT_POWER_ID 'P'
#define TRANSMIT_POWER_0 '0'
#define TRANSMIT_POWER_1 '1'
#define TRANSMIT_POWER_2 '2'
#define TRANSMIT_POWER_3 '3'
#define TRANSMIT_POWER_4 '4'
#define TRANSMIT_POWER_5 '5'
#define TRANSMIT_POWER_6 '6'
#define TRANSMIT_POWER_7 '7'
#define TRANSMIT_POWER_8 '8'
#define TRANSMIT_POWER_9 '9' //DEFAULT

//Power saving settings definition
//The number stands for the amount of time the receiver/transmitter will be on
//	On Time				Typical Average Receiver Current
//	100%					16mA
//	12.50%				2mA
//	6.25%					1mA
//	3.33%					500uA
//	1.56%					250uA
//	0.78%					125uA
//	0.39%					63uA
//	0.20%					32uA
//	0%						Radio receiver is completely disabled and turned off. Radio stays in Idle and Sleep mode and only turns on to transmit data when requested
#define RECEIVER_POWER_SAVING_ID 'D'
#define TRANSMITTER_POWER_SAVING_ID 'd'
#define POWER_100 '0' //DEFAULT
#define POWER_12_50 '1'
#define POWER_6_25 '2'
#define POWER_3_33 '3'
#define POWER_1_56 '4'
#define POWER_0_78 '5'
#define POWER_0_39 '6'
#define POWER_0_20 '7'
#ifdef V_1_5
	#define POWER_0 '8'
#endif

//Frequency settings definition
#define FREQUENCY_ID 'F'

//Channel settings definition
//	Channel			Frequency
//	0						434.000MHz
//	1						434.100MHz
//	2						434.200MHz
//	3						434.300MHz
//	4						434.400MHz
//	5						434.500MHz
//	6						434.600MHz
//	7						434.700MHz
//	8						434.800MHz
//	9						434.900MHz
#define CHANNEL_ID 'C'
#define CHANNEL_0 '0' //DEFAULT
#define CHANNEL_1 '1'
#define CHANNEL_2 '2'
#define CHANNEL_3 '3'
#define CHANNEL_4 '4'
#define CHANNEL_5 '5'
#define CHANNEL_6 '6'
#define CHANNEL_7 '7'
#define CHANNEL_8 '8'
#define CHANNEL_9 '9'


#ifdef V_1_5
//Low power settings definition
/*
Mode 1 : eRIC_LPM_Level0() is enabled. Clock is set to 1048576Hz and D7
(RxPower level) is enabled. Pin22 is set as Interrupt to exit this mode.
All other interrupts are disabled except UART, Pin22 and Radio. eRIC
can send data, receive data and update settings.
Current consumption ~180uA

Mode 2 : eRIC_LPM_Level1() is enabled. Clock is set to 32768Hz and D7
(RxPower level) is enabled. Pin22 is set as Interrupt to exit this mode.
All other interrupts are disabled except UART, Pin22 and Radio. eRIC
can send data and receive data. Can also update settings but may
struggle to update EEprom and other settings as clock is on low speed.
Current consumption ~37uA.
If ER_CMD#D8 (Radio receiver Off) and ER_CMD#d8 is used (Can
send data but not receive data)
Current consumption ~6uA

Mode 3 : eRIC_LPM_Level2() is enabled. All clocks are turned off. Radio is off.
Pin22 interrupt is enabled to exit this mode.
Current consumption ~2uA
*/
#define LOW_POWER_MODE_ID "A2"
#define LOW_POWER_MODE_1 '1'
#define LOW_POWER_MODE_2 '2'
#define LOW_POWER_MODE_3 '3'

//AES encryption settings definition
#define AES_ID "A1"
#define AES_DISABLED '0'
#define AES_ENCRYPTION_DECRYPTION_ENABLED '1'
#define AES_ENCRYPTION_ENABLED '2'
#define AES_DECRYPTION_ENABLED '3'
#endif

class RF{
	public :
		RF();
		RF(BufferedSerial *);

		void init();
		bool setDefaultSettings();
		void enableCheckSettings();
		void enableRetry(uint8_t);
		void beginDebug(Serial *);

		bool setUARTBaudRate(uint8_t);
		uint8_t getUARTBaudRate();
		bool setDataBaudRate(uint8_t);
		uint8_t getDataBaudRate();
		bool setTransmitPower(uint8_t);
		uint8_t getTransmitPower();

		bool setChannel(uint8_t);
		uint8_t getChannel();
		bool setReceivePowerSaving(uint8_t);
		float getReceivePowerSaving();
		bool setTransmitPowerSaving(uint8_t);
		float getTransmitPowerSaving();

		#ifdef V_1_5
		bool setFrequency(unsigned int); //Set frequency in Hz
		long getFrequency();
		bool setAESEncryptionMode(uint8_t, std::string key);
		uint8_t getAESEncryptionMode();
		bool setLowPowerMode(uint8_t);
		uint8_t getPowerMode();
		#endif

		bool changeSetting(uint8_t, uint8_t);
		bool changeSetting(uint8_t, std::string); //Overload for setFrequency function
		bool changeSetting(std::string, uint8_t); //Overload for setLowPowerMode function
		bool changeSetting(std::string, std::string); //Overload for setAESEncryptionMode function
		bool sendCommand(std::string);
		std::string getResponse();

		void sendFrame(std::string);

	private :
		bool _checkSetting;
		bool _printDebug;
		uint8_t _retry;

		BufferedSerial * _RFSerial;
		Serial * _debugSerial;

		uint8_t _UARTBaudRate;
		uint8_t _dataRate;
		uint8_t _transmitPower;
		long _frequency;
		uint8_t _channel;
		uint8_t _receivePowerSaving;
		uint8_t _transmitPowerSaving;
		uint8_t _lowPowerMode;
		uint8_t _AESEncryptionMode;
		std::string _cmd;
		std::string _checkCmd;

};
