As part of a project, I had to use the eRIC4 module from LPRS, which is a module for sending data over radio frequencies.

The module can simply be configured via UART, by sending formated string on a serial port. (See the datasheet for more information)

For more conveniance for my project, I needed to develop a library in C++, which basically just send strings on the chosen port.